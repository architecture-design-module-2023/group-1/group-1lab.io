workspace "Getting Started" "This is a model of my software system." {

    model {
    
        Administrador = person "Administrador" "Personal designado como administrador"
        Estudiante = person "Estudiante" "Estudiante matriculado"
        Docente = person "Docente" "Docente habilitado asignado a uno o varios cursos"
        Administrativo = person "Administrativo" "Personal administrativo"
        


        softwareSystem = softwareSystem "Gestión de calificaciones en escuelas " "softwareSystem" {
            webPage = container "Pagina Web" "Muestra tdo el contenido estatico"
            spaAdministrador = container "Single Page Application de Administrador" "Provee toda la informacion del usuario"
            spaEstudiante = container "Single Page Application de Personal Estudiante" "Provee toda la informacion del usuario"
            spaDocente = container "Single Page Application de Personal Docente" "Provee toda la informacion del usuario"
            spaAdministrativo = container "Single Page Application de Personal Administrativo" "Provee toda la informacion del usuario"
           
            database = container "Base de Datos" "Provee toda la informacion del usuario" "Database"
            firewall = container "Regla firewall" "Contiene las reglas por IP" "rules"
            apiApplication = container "API" "Provee toda la informacion del usuario"{
                user = component "Controlador de Usuario"
                userAuthentication = component "Controlador autenticacion de Usuario"
                exam = component "Controlador de examenes" "Controla el sistema de examenes"
                score = component "Controlador de calificaciones"
                reports = component "Controlador de reportes"
        
                userService = component "Servicio de Usuarios"
                authService = component "Servicio de autenticacion de Usuarios"
                examService = component "Servicio de examenes"
                scoreService = component "Servicio de calificaciones" "Se encarga de la administración de notas"
                reportsService = component "Servicio de reportes" "Se encarga de la administración de reportes"
                
                /*Conexiones*/
                user -> userService "Utiliza"
                userAuthentication -> authService "Utiliza"
                exam -> examService "Utiliza"
                score -> scoreService "Utiliza"
                reports -> reportsService "Utiliza"
   
                userService -> database "Se conecta"
                reportsService -> database "Se conecta"
                authService -> firewall "Se conecta"
                examService -> database "Se conecta"
                scoreService -> database "Se conecta"
                
                firewall -> database "Se conecta"
                
                Administrador -> webPage  "Administra el acceso de los centros por IP, Crea usuarios, Asigna roles"
                Docente -> webPage  "Crea examenes, ingresa notas, calificación de examenes manuales, aprobación / negación  cambio de calificación, ver reporte grupal"
                Estudiante -> webPage  "Rinde examen, verifica notas, petición cambio calificación, ver su reporte de notas"
                Administrativo -> webPage  "Verificación solicitud recalificación, informe de tendencias del estudiante, informe de tendencias del docente"
                
            }
            
            webPage -> spaAdministrador "Entrega el contenido al navegador del usuario"
            webPage -> spaEstudiante "Entrega el contenido al navegador del usuario"
            webPage -> spaDocente "Entrega el contenido al navegador del usuario"
            webPage -> spaAdministrativo "Entrega el contenido al navegador del usuario"
            apiApplication -> database "Lectura y escritura de datos" "TCP"
            spaAdministrador -> apiApplication "Hace llamadas a la API" "HTTP/Websocktes"
            spaEstudiante -> apiApplication "Hace llamadas a la API" "HTTP/Websocktes"
            spaDocente -> apiApplication "Hace llamadas a la API" "HTTP/Websocktes"
            spaAdministrativo -> apiApplication "Hace llamadas a la API" "HTTP/Websocktes"

            

            spaAdministrador -> user "llama a la API" "HTTP"
            spaAdministrador -> userAuthentication "llama a la API" "HTTP"
            
            spaDocente -> exam "llama a la API" "HTTP"
            spaEstudiante -> exam "llama a la API" "HTTP"
            spaAdministrativo -> exam "llama a la API" "HTTP"
            
            spaDocente -> userAuthentication "llama a la API" "HTTP"
            spaEstudiante -> userAuthentication "llama a la API" "HTTP"
            spaAdministrativo -> userAuthentication "llama a la API" "HTTP"
            
            spaDocente -> reports "llama a la API" "HTTP"
            spaEstudiante -> reports "llama a la API" "HTTP"
            spaAdministrativo -> reports "llama a la API" "HTTP"
            
            spaDocente -> score "llama a la API" "HTTP"
            spaEstudiante -> score "llama a la API" "HTTP"
            spaAdministrativo -> score "llama a la API" "HTTP"
            
            
            
        }
        

    }

    views {
     
        container softwareSystem {
            include *
            autolayout lr
        }
        component apiApplication {
            include *
            autolayout lr
        }
        systemContext softwareSystem "SystemContext" "An example of a System Context diagram." {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            
            element "Database" {
                shape cylinder
                background #08427b
                color #ffffff
            }
            
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            
            element "GoogleCloud" {
                background #A9A9A9
                color #000000
            }
        }
    }
    
}