># Aplicación web para la gestión de calificaciones en escuelas públicas

># Autores
- Diana Raquel Diaz Tapia
- Felipe Marcelo Serrano Flores
- Angel Javier Parra Bagua
- Ernesto Alonso Robles Peñaherrera

># Descripción

Esta es la solución propuesta para la gestión de calificaciones en escuelas públicas

Las funciones del administrador en la aplicación son las siguientes:
- Administra el acceso de los centros por IP
- Crear usuarios
- Asignar rol: Estudiantes ==> Cursos
- Asignar rol: Docentes ==> Asignatura y Cursos
- Asignar rol: Administrativos ==> Departamento/Área

Las funciones del estudiante son las siguientes:
- Rinde examen
- Verifica notas
- Petición cambio calificación
- Ve su reporte de notas

Las acciones del docente son las siguientes: 
- Crea exámenes
- Ingresa notas
- Calificación de exámenes manuales
- Aprobación/ Negación cambio de calificación
- Ver reporte grupal

Las acciones del administrativo son las siguientes: 
- Verificación solicitud recalificación
- Informe de tendencias del estudiante
- Informe de tendencias del docente

># Decisiones de arquitectura

La documentación de decisiones de arquitectura o Architecture Decision Records (ADR) es una colección de documentos que recogen individualmente cada una de las decisiones de arquitectura tomadas. 

|  A continuación las de esta solución |
|---|
| [Lenguaje para escribir documentos](adr/idioma/index.md)|
| [Arquitectura de la solución](adr/arquitectura/index.md) |
| [Repositorio de datos](adr/datos/index.md) |
| [Interfaz de usuario](adr/frontend/index.md) |

># Diagramas
Los diagramas estarán plasmados por el [modelo c4](diagramas/diagramas-c4.dsl)
Estos diagramas proporcionan diferentes niveles de abstracción, cada uno de los cuales es relevante para una audiencia diferente.

| A continuación los de esta solución |

- [contexto](diagramas/contexto.PNG)
- [contenedores](diagramas/Container.PNG)
- [componentes](diagramas/Components.png)

># Pila de tecnología
- [Node JS](Tecnología/node.md)
- Express JS
- Angular JS
- PostgreSQL
- Material
- Azure


