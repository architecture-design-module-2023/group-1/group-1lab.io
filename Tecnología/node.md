# Node JS
Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
La aplicación debe ser accesible desde cualquier dispositivo con conexión a Internet y un navegador web, esto significa que los usuarios pueden acceder a la aplicación desde una computadora de escritorio, una computadora portátil, una tableta o un teléfono inteligente, sin necesidad de instalar software adicional. 

## Decisión
Las aplicaciones web se distribuyen a través de Internet, lo que facilita la actualización y distribución de nuevas versiones o funcionalidades, los usuarios pueden acceder a la última versión de la aplicación sin necesidad de descargar e instalar actualizaciones manualmente, lo que simplifica la experiencia de usuario y permite una iteración más rápida del desarrollo.

Node.js es un entorno de ejecución de JavaScript, se utiliza principalmente para desarrollar aplicaciones del lado del servidor y proporciona un entorno de tiempo de ejecución rápido y eficiente para ejecutar código JavaScript fuera del navegador, node es adecuado para:

- Enfoque de eventos y a la capacidad de manejar múltiples conexiones de manera eficiente.
- Tiene un enfoque de eventos y a la capacidad de manejar múltiples conexiones de manera eficiente, es una buena opción para implementar microservicios y arquitecturas orientadas a servicios debido a su capacidad para manejar múltiples solicitudes concurrentes de manera eficiente.
- Su naturaleza JavaScript unificada tanto en el lado del cliente como en el servidor permite un desarrollo más fluido y una fácil comunicación entre el backend y el frontend.

## Consecuencia

- Si una solicitud requiere una operación de bloqueo intensivo, puede afectar el rendimiento general del servidor.
- Utiliza devoluciones de llamada para el manejo de errores y el control del flujo
- Para escalabilidad horizontal y distribución de carga en múltiples servidores, se pueden requerir soluciones adicionales y estrategias adecuadas.

## Estado
Propuesto

# Última actualización

2023-07-06