# Frontend

### Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
>>Se requiere desarrollar una aplicación web que se ocupara en la escuela, no se vio la necesidad de tener una aplicación móvil y por la limitante del presupuesto.

## Decisión
>>Programación con Framework (ANGULAR)

## Consecuencia
>>* Un repositorio más para el proyecto donde se aloja el frontend
>>* Capacitar al equipo Framework (ANGULAR)

## Estado
>>Aprobado



