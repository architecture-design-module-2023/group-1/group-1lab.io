# Idioma a utilizar para documentar

### Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
>>El equipo de trabajo maneja el lenguaje español con mas facilidad, entonces la documentación se realizara en ese idioma.

## Decisión
>>Documentar todo en español

## Consecuencia
>>* Facil entendimiento en el equipo de trabajo
>>* Problemas al momento de tener un miembro del equipo o cliente extranjero
>>* Si otros sistemas similares ofrecen soporte en varios idiomas, es posible que no tengamos competitividad. 

## Estado
>>Aprobado
