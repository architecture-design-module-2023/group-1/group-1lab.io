#   Administración de accesos al uso de la plataforma mediante IP pública (Requerimiento de aislamiento de uso de plataforma)

## Estado

Propuesto

## Contexto   
>>Los estudiantes solo podrán usar la aplicación dentro de los centros de evaluación de todo el estado, los cuales fueron seleccionado. 

## Decisiones
>>Se toma como decisión que solo puedan acceder a los centro autorizados por el Estado. Los cuales solo se habilitaran por medio del rango de las IP PUBLICA de cada centro.
Arquitectura Cliente Servidor

## Consecuencias
>>- Saturaciones concentradas en los centros habilitados.
>>- Verificación
>>- Los estudiantes estan obligados a rendir la prueba en los centros autorizados
>>- Saturación de los servicios de PROXY y de FIREWALL
>>- Actualización constantes de la lista blanca de las direcciones IP
>>- Verificación constante de las IP conectadas
>>- Programas especialidados

## Ultima actualización

2023-06-29