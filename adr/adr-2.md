#  Administración de Escalabilidad de plataforma

## Estado
Propuesto

## Contexto
>>Es esencial asegurarnos de que la plataforma pueda gestionar de manera eficiente y escalable la carga adicional. Con una cantidad de 40,000 estudiantes, 2,000 docentes y 50 administrativos, alcanzamos un total de 42,050 usuarios involucrados en el sistema.

## Decisiones
>>Se toma como decisión implementar una arquitectura escalable para la plataforma, utilizando tecnologías y prácticas que permitan aumentar la capacidad de procesamiento y almacenamiento según sea necesario. Algunas decisiones clave son:
>>- Uso de servicios en la nube Azure.
>>- Autoscaling: Se va a escalar cuando el CPU tenga un 80% de su capacidad, sin pasar del presupuesto mensual.
>>- Se va a usar un escalamiento horizontal.

## Consecuencias
>>- Capacidad de escalar la plataforma según la demanda, lo que permite manejar un mayor número de usuarios y cargas de trabajo.
>>- Mayor disponibilidad y confiabilidad al distribuir la carga entre múltiples instancias y utilizar servicios en la nube.
>>- Posible aumento de los costos debido al uso de servicios en la nube y la necesidad de recursos adicionales para escalar.
>>- Se puede asignar reglas de escalamiento como la reducción de carga los fines de semana al bajar el número de conexiones de usuarios.

## Ultima actualización
2023-06-29