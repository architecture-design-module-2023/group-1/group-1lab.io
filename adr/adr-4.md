# Autorización de usuarios

## Estado

Propuesto

## Contexto

>>Se ha identificado que las opciones disponibles actualmente no son capaces de gestionar todos los usuarios de manera efectiva. Esto podría ocasionar alteraciones en los resultados o en el proceso general del sistema. Por lo tanto, es necesario implementar un mecanismo de verificación del rol de cada usuario que intenta acceder al sistema, con el fin de mostrar únicamente el menú correspondiente a sus privilegios..

## Decisiones
En vista de la necesidad de gestionar adecuadamente los usuarios del sistema, se ha tomado la decisión:
>>- Implementar un mecanismo de autorización al ingresar al sistema.
>>- Establecer roles de usuario.
>>- Implementar autenticación segura.
>>- Aplicar políticas de acceso.
>>- Registrar actividades de usuarios

## Consecuencias
La implementación de esta decisión conllevará las siguientes consecuencias:



#### - Solo se mostrará el menú y las opciones pertinentes al rol y perfil de cada usuario.
#### - Los usuarios no podrán realizar acciones que estén fuera de su perfil autorizado, lo que contribuirá a mantener la integridad y seguridad del sistema.
#### - Mayor seguridad y protección de datos:
>>##### El mecanismo de autorización y autenticación fortalecerá la seguridad del sistema, evitando accesos no autorizados y protegiendo la confidencialidad de los datos de los estudiantes.
#### - Cumplimiento normativo
>>##### Al involucrar agencias gubernamentales en el proceso de aprobación de cambios y garantizar la seguridad de las calificaciones de los estudiantes, se cumplirán los requisitos normativos y legales establecidos por el estado.
#### - Mayor control y gestión de usuarios
>>##### Las políticas de acceso y los roles definidos permitirán un control más efectivo sobre las acciones que los usuarios pueden realizar en el sistema, minimizando el riesgo de errores o mal uso.
#### - Mejor rendimiento y experiencia del usuario
>>##### Al mostrar solo las opciones y funcionalidades relevantes para cada usuario, se mejorará la usabilidad y eficiencia del sistema, facilitando la navegación y reduciendo la sobrecarga de información.

## Ultima actualización

2023-06-29