# Justificación de presupuesto (manejo de presupuesto)

## Estado
Propuesto

## Contexto
>>Se requiere un proceso de aprobación de cambios que involucre a tres agencias gubernamentales diferentes para cambios en la forma en que se mantienen las calificaciones de los estudiantes para garantizar la seguridad.

>>El estado no es dueño de su centro de hospedaje, sino que lo subcontrata a un tercero. El proyecto debe defender su presupuesto cada año fiscal.

## Decisiones
>>Para el escalamiento horizontal será en Azure, se registra la decisión de utilizar Azure como proveedor de servicios en la nube, implica aprovechar las capacidades de escalado vertical que proporciona.

>>Se escogerá la opción de administración con Azure App Service para la implementación de Angular dado a que nos proporciona capacidades de escalado automático que te permiten aumentar o disminuir la capacidad de tus instancias de aplicación en función de la carga de trabajo; debido a que nos permite aumentar la capacidad de CPU y memoria de cada instancia para manejar cargas de trabajo más intensivas o mejorar el rendimiento en momentos de alta demanda

## Consecuencias
>>- Optar por opciones de recursos más limitadas
>>- Limitaciones en la capacidad para manejar picos de tráfico o volúmenes de datos más grandes.
>>- Dependencia de recursos de código abierto y requerir más tiempo y esfuerzo para encontrar soluciones y resolver problemas.
>>- Riesgo en las medidas de seguridad avanzadas o redundancia en caso de fallas

## Ultima actualización

2023-06-29
