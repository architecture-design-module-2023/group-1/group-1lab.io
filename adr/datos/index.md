# Repositorio de datos

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
>>Se requiere una base de datos relacional que brinde funcionalidad y características avanzadas, escalabilidad, confiabilidad y estabilidad, seguridad, comunidad y soporte: PostgreSQL cuenta con una activa comunidad de usuarios y desarrolladores en todo el mundo.

## Decisión
>>Optamos por Postgress es una base de datos relacional de código abierto ampliamente utilizada y valorada por varias razones. Aquí te presento algunas razones por las cuales elegir PostgreSQL como base de datos: Funcionalidad y características avanzadas, escalabilidad, confiabilidad y estabilidad, seguridad, comunidad y soporte: PostgreSQL cuenta con una activa comunidad de usuarios y desarrolladores en todo el mundo.
## Consecuencia
>>- Fiabilidad y estabilidad
>>- Amplio conjunto de características
>>- Escalabilidad
>>- Alto rendimiento
>>- Seguridad
>>- Licencia de código abierto
>>- Comunidad activa y soporte:

## Estado
>>Aprobado


