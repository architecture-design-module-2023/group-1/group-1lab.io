# Arquitectura en Capas

Contenido:
* [Contexto](#contexto)
* [Decisión](#decision)
* [Consecuencia](#consecuencia)
* [Estado](#estado)

## Contexto
>>Al tener un presupuesto limitado, se debe buscar maximizar el valor de la inversión a realizar y optimizar los recursos disponibles, que se adapte fácilmente a los cambios en los requisitos o a nuevas funcionalidades que puedan surgir con el tiempo, sin embargo, se requiere utilizar un estilo de arquitectura que tenga independecia de responsabilidades, abstración, flexibilidad, reutilización, escalabilidad y desarrollo en paralelo.

## Decisión
>>- Se utilizará un estilo de arquitectura por capas, el cual proporciona una estructura organizada y modular, permitiendo la separación de responsabilidades, reutilización de componentes, es adaptable y escalable lo que facilita el desarrollo en el mantenimiento y la evolución del sistema a largo tiempo.
>>- Separa Responsabilidades, cada capa se enfoca a una tarea específica 
>>- Modularidad y reutilización, cada capa construye un módulo independiente que puede ser utilizado por otras capas, esto permite que los componentes puedan ser diseñados y probados de forma aislada antes de ser integrados.
>>- Flexibilidad y adaptabilidad: La arquitectura en capas facilita la incorporación de nuevos requisitos y cambios en el sistema, separación de responsabilidades, es más fácil agregar nuevas funcionalidades o modificar las existentes sin afectar otras partes del sistema,permite una mayor flexibilidad y adaptabilidad a medida que los requisitos del negocio evolucionan con el tiempo.
>>- Al tener la posibilidad que cada capa se pueda probar de forma aislada, simplifica la identificación y resolución de problemas.
## Las capas son: 
>>- Logica de presentación
>>- Logica de negocio
>>- Logica de persistencia de datos

## Consecuencia
>>- Rendimiento reducido: La sobrecarga de comunicación puede generar un aumento en el tiempo de respuesta de la aplicación debido a la necesidad de procesar y transferir datos entre capas. Esto puede afectar negativamente la experiencia del usuario, especialmente en situaciones de alto tráfico o con grandes volúmenes de datos.
>>- Mayor complejidad: La comunicación entre capas puede agregar complejidad al sistema, ya que se requiere una gestión adecuada de los mensajes y la sincronización entre las capas. Esto puede dificultar la comprensión, el mantenimiento y la depuración del sistema.
>>- Posible dependencia excesiva: Si una capa depende demasiado de otras capas para realizar sus funciones, puede generar acoplamiento y dificultar la modificación o sustitución de una capa sin afectar a las demás. Esto puede limitar la flexibilidad y la escalabilidad del sistema.
>>- Requiere de una mayor planificación y diseño inicial
>>- La comunicación entre capas puede generar una carga adicional 
>>- En caso de que las capas no estén adecuadamente diseñados y acoplados, puede hacer más difícil mantener y modificar el sistema. 
>>- Puede ocurrir duplicación lógica.
>>- Si un cambio requiere modificaciones en múltiples capas, puede resultar más complejo y costoso implementarlo.

## Estado
>>Aprobado

# Última actualización

2023-07-11
